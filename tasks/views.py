from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, UpdateView
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.


def list_task(request):
    context = {}
    if request.user.is_authenticated:
        context["tasks"] = Task.objects.filter(assignee=request.user)
        return render(request, "list_tasks.html", context)
    else:
        return redirect("login")


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "create_task.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskUpdateView(UpdateView):
    model = Task
    template_name = "list_tasks.html"
    fields = ["is_completed"]

    def get_success_url(self):
        return reverse_lazy("show_my_tasks")
