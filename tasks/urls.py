from django.urls import path
from tasks.views import TaskCreateView, TaskUpdateView, list_task

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", list_task, name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
]
